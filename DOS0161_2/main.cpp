#include <iostream>
#include "HashTable.h"

int main() {
	HashTable* t = new HashTable();
	t->insert("ahoj");
	t->insert("nazdar");
	t->insert("cus");
	t->insert("jak je?");
	t->insert("hello");
	t->insert("zdarec");
	t->insert("cauky");
	t->insert("hola");
	t->insert("ciao");
	t->insert("hoj");

	t->report();
	
	string str;
	cout << "search for: ";
	cin >> str;
	cout << boolalpha << t->search(str) << endl;

	system("pause");
	return 0;
}