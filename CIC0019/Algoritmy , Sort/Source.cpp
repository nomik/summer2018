#include <iostream>
#include <string>
#include <vector>

using namespace std;


struct ListItem
{
	int Value;
	ListItem* Prev;
	ListItem* Next;
};

struct List
{
	ListItem* Head;
	ListItem* Tail;
};

bool IsEmpty(const List& L)
{
	return L.Head == nullptr;
}

void InternalCreateSingleElementList(List& L, const int NewValue)
{
	L.Head = L.Tail = new ListItem;
	L.Head->Value = NewValue;
	L.Head->Prev = nullptr;
	L.Head->Next = nullptr;
}


void Append(List& L, const int NewValue)
{
	if (IsEmpty(L))
	{
		InternalCreateSingleElementList(L, NewValue);
	}
	else
	{
		ListItem* NewItem = new ListItem;
		NewItem->Value = NewValue;
		NewItem->Next = nullptr;
		L.Tail->Next = NewItem;
		NewItem->Prev = L.Tail;
		L.Tail = NewItem;
	}
}

void Init(List& L)
{
	L.Head = nullptr;
	L.Tail = nullptr;
}


void Init(List& L, const int myVector[], const int N)
{
	Init(L);
	for (int i = 0; i < N; i++)
	{
		Append(L, myVector[i]);
	}
}






void Init(List& L, vector <int>& myVector, const int N)
{
	Init(L);
	for (int i = 0; i < N; i++)
	{
		Append(L, myVector.at(i));
	}
}






void ReportStructure(const List& L)
{
	cout << "L.Head: " << L.Head << endl;
	cout << "L.Tail: " << L.Tail << endl;
	for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		cout << "Item address: " << p << endl;
		cout << "Value: " << p->Value << endl;
		cout << "Prev: " << p->Prev << endl;
		cout << "Next: " << p->Next << endl;
		cout << endl;
	}
}


//uloha
void Sort(List &L) {
	ListItem *a = L.Head;
	ListItem *b = L.Tail;

	for (a; a != b->Next; a = a->Next) {

		ListItem *x = a;
		for (ListItem *c = a->Next; c != b->Next; c = c->Next) {
			if ((c->Value) < (x->Value)) {
				x = c;
			}
		}
		int pomoc = x->Value;
		x->Value = a->Value;
		a->Value = pomoc;
	}
}

int main()
{
	List x;
	const int N = 10;
	int pole[] = { 6,25,18,20,19,10,5,8,9,7 };
	Init(x, pole, N);
	Sort(x);
	ReportStructure(x);
	system("pause");
	return 0;
}