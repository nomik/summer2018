#include <iostream>
#include "List.h"
#include <string>

using namespace std;

int main()
{
    List *L = new List;
    const int n = 6;
    const string Values[6] = {"Jedna", "Dva", "Tri", "Ctyri", "Pet", "Sest"};
    Init(*L, Values, n);

    Append(*L, "Sedm");
    Prepend(*L, "Nula");

    ListItem *S = Search(*L, "Tri");
    InsertAfter(*L, S, "A");

    Report(*L);

    Remove(*L, S);
    Report(*L);

    Remove(*L, Search(*L, "Nula"));
    Remove(*L, Search(*L, "Sedm"));
    Append(*L, "C");
    Prepend(*L, "B");
    Report(*L);

    Clear(*L);
    Append(*L, "Nova Hodnota");
    Report(*L);

    return 0;
}
