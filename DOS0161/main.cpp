/** @File: main
 * @Author: Josef Dostál
 * @Date: 19.03.2018
 */
#include <iostream>
#include "List.h"

using namespace std;

void getTestNum(int& choice, int& numOfItems) {
	cout << "Zadejte pocet vzorku: ";
	cin >> numOfItems;
	cout << "[1] Insert(List&, int) test" << endl;
	cout << "[2] Init(List&, int[], int) test" << endl;
	cout << "[3] Init(List&, List&) test" << endl;
	cout << "[4] Search(List&, int) test" << endl;
	cout << "[5] ReverseSearch(List&, int) test" << endl;
	//add tests
	cin >> choice;
	cout << "***************" << endl;
}

List InsertTest(List l, int numOfItems) {
	for (int i = 0 ; i < numOfItems ; i++) {
		int randNum;
		do {
			randNum = rand()%(numOfItems*2);
		} while (Search(l, randNum) != nullptr);
		Insert(l, randNum);
	}
	ReportStructure(l);
	return l;
}

bool isInArr(int num, int arr[], int N) {
	for (int i = 0 ; i < N ; i++) {
		if (num == arr[i])
			return true;
	}
	return false;
}

List InitTest1(List l, int numOfItems) {
	int arr[numOfItems];
	for (int i = 0 ; i < numOfItems ; i++) {
		int randNum;
		do {
			randNum = rand()%(numOfItems*2);
		} while (isInArr(randNum, arr, i));
		arr[i] = randNum;
	}
	Init(l, arr, numOfItems);
	ReportStructure(l);
	return l;
}

void InitTest2(List l, int numOfItems) {
	List inserted;
	Init(inserted);
	for (int i = 0 ; i < numOfItems ; i++) {
		int randNum;
		do {
			randNum = rand()%(numOfItems*2);
		} while (Search(inserted, randNum) != nullptr);
		Insert(inserted, randNum);
	}
	Init(l, inserted);
	cout << "Original List:" << endl;
	ReportStructure(inserted);
	cout << "*****************" << endl;
	cout << "New List:" << endl;
	ReportStructure(l);
}

void SearchTest(List l, int numOfItems) {
	cout << "Seznam:" << endl;
	l = InsertTest(l, numOfItems);
	cout << "*****************" << endl;
	cout << "Vyhledat prvek o hodnote: ";
	int n;
	cin >> n;
	ListItem* I = Search(l, n);
	if (I != nullptr)
		ReportItem(I);
	else
		cout << "Prvek nenalezen" << endl;
}

void ReverseSearchTest(List l, int numOfItems) {
	cout << "Seznam: " << endl;
	l = InsertTest(l, numOfItems);
	cout << "*****************" << endl;
	cout << "Vyhledat prvek o hodnote: ";
	int n;
	cin >> n;
	ListItem* I = ReverseSearch(l, n);
	if (I != nullptr)
		ReportItem(I);
	else
		cout << "Prvek nenalezen" << endl;
}

int main() {
	srand(time(NULL)*rand());
	List l;

	int choice, numOfItems;
	getTestNum(choice, numOfItems);

	switch (choice) {
		case 1:
			Init(l);
			InsertTest(l, numOfItems);
			break;
		case 2:
			InitTest1(l, numOfItems);
			break;
		case 3:
			InitTest2(l, numOfItems);
			break;
		case 4:
			Init(l);
			SearchTest(l, numOfItems);
			break;
		case 5:
			Init(l);
			ReverseSearchTest(l, numOfItems);
			break;
		default:
			cout << "Spatna volba" << endl;
	}

	return 0;
}
